# Exploratory Software Product for Comp5200 Final Year Project
## Developed by  
Name: Maaike de Jong  
Student ID: sc19mald  
Email: sc19mald@leeds.ac.uk

## Developed with
IDE: IntelliJ IDEA 2020.1.3
Java Version: 11

## Installation Instructions
To install the plugin:
- Download the repository, unzip it and open the project in IntelliJ.  
- In the top right of the tool bar, next to the little green hammer, select 'Add configuration.'  
- Select 'Templates' and choose 'Gradle', then under configuration, set the Gradle Project to this project.  
- When you press the Gradle icon on the bar along the right side of the screen, you should now see the project name. 
- Press the project name, and you'll be presented with a list of tasks.  
- From this list, select intellij then 'buildPlugin'. 
- Once it has finished building, select 'runIde' from this menu.
- Now open a project of your choice. The plugin's functionality is found under "Code Smells" along the tool bar.
