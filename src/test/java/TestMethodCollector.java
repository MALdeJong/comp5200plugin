import com.CodeSmellDetector.Visitors.MethodCollector;
import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.MethodDeclaration;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class TestMethodCollector {
    private CompilationUnit cu;

    @BeforeAll
    public void init() {
        ClassLoader classLoader = getClass().getClassLoader();
        File mockClassFile = new File(classLoader.getResource("MockClass").getFile());
        Path mockClassPath = Paths.get(mockClassFile.getPath());

        try {
            this.cu = createCompilationUnit(mockClassPath);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testMethodCollector() {
        List<MethodDeclaration> resultList = new ArrayList<>();
        MethodCollector methodCollector = new MethodCollector();

        methodCollector.visit(cu, resultList);
        assertTrue(resultList.size() == 7);
    }

    private CompilationUnit createCompilationUnit(Path javaFileLocation) throws Exception {
        return StaticJavaParser.parse(new File(String.valueOf(javaFileLocation)));
    }

}
