import com.CodeSmellDetector.LongFunctionSmellSniffer;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class TestLongFunctionSmellSniffer {
    private List<String> methodList = new ArrayList<>();

    @BeforeAll
    void init() {
        LongFunctionSmellSniffer smellSniffer = new LongFunctionSmellSniffer();
        ClassLoader classLoader = getClass().getClassLoader();
        File mockClassFile = new File(classLoader.getResource("MockClass").getFile());
        Path mockClassPath = Paths.get(mockClassFile.getPath());
        smellSniffer.analyse(mockClassPath);

        if (smellSniffer.getIssueList().size() > 0) {
            smellSniffer.getIssueList().forEach(e ->
                    methodList.add(e.getElementIdentifier()));
        }
    }

    @Test
    public void dontDetectExactlyThirty() {
        assertFalse(methodList.contains("exactlyThirty()"));
    }

    @Test
    public void detectMoreThanThirty() {
        assertTrue(methodList.contains("longerThanThirty()"));
    }

    @Test
    public void dontDetectBelowThirty() {
        assertAll(
                () -> assertFalse(methodList.contains("shorterThanThirty()")),
                () -> assertFalse(methodList.contains("emptyFunction()"))
        );
    }

    @Test
    public void dontCountComments() {
        assertFalse(methodList.contains("functionWithComments()"));
    }

    @Test
    public void dontCountBlankLines() {
        assertFalse(methodList.contains("functionWithBlankLines()"));
    }

    @Test
    public void dontCountCommentsOrBlankLines() {
        assertFalse(methodList.contains("functionWithCommentsBlankLines()"));
    }
}
