/**
 *  @author Maaike de Jong
 *  The contents of this class were written as part of
 *  the COMP5200 Final Year Project Module.
 */
package com.MenuHandler;

import com.CodeSmellDetector.CodeElement;
import com.CodeSmellDetector.LongFunctionSmellSniffer;
import com.ResultPane.ResultToolWindowManager;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowManager;
import com.intellij.ui.content.Content;
import org.jetbrains.annotations.NotNull;

import java.nio.file.Paths;
import java.util.List;

import static java.util.Objects.isNull;

/**
 * Handles the Long Function menu choice.
 */
public class ActionDetectLongFunction extends AnAction {
    /**
     * Reference to the currently opened plugin.
     */
    private Project project;

    /**
     *
     * This method handles the user selection of the "long function" code smell.
     *
     * @param e AnActionEvent that holds
     *          the project's context at
     *          the time of the action event.
     */
    @Override
    public void actionPerformed(@NotNull AnActionEvent e) {
        setProject(e.getProject());

        // If no editor is currently selected, display error message and return.
        Editor editor = e.getData(CommonDataKeys.EDITOR);
        if (isNull(editor)) {
            Messages.showMessageDialog(project, "Unable to locate the project editor. Please open a Java file in the editor.", "Error: Editor not found", null);
            return;
        }

        // If the file currently open in the editor is a .java file, analyse with LongFunctionSmellSniffer, then send results to plugin ToolWindow.
        VirtualFile file = FileDocumentManager.getInstance().getFile(editor.getDocument());
        if (file.getName().endsWith(".java")) {
            LongFunctionSmellSniffer longFunctionSmellSniffer = new LongFunctionSmellSniffer();
            longFunctionSmellSniffer.analyse(Paths.get(file.getPath()));
            updateToolWindow(longFunctionSmellSniffer.getIssueList(), file);
        } else {
            // If the file currently open in the editor is NOT a .java file, display error message and return.
            Messages.showMessageDialog(project, file.getName() + " is not a Java file. Please open a Java file in the editor.", "Error: Not a Java File", null);
            return;
        }
    }

    /**
     *
     * This method sets the class' project attribute to be the currently opened project.
     *
     * @param project The project that is
     *                currently open in the editor.
     */
    private void setProject(Project project) {
        this.project = project;
    }

    /**
     *
     * This method updates the Result Tool Window with a list of all discovered code smell bearing code elements.
     *
     * @param issueList A list of CodeElements bearing the
     *                  the long function code smell.
     * @param file      The file from which the
     *                  issueList originates.
     */
    private void updateToolWindow(List<CodeElement> issueList, VirtualFile file) {
        // Find the plugin's tool window, as registered in plugin.xml.
        ToolWindow toolWindow = ToolWindowManager.getInstance(project).getToolWindow("Code Smell Search");

        // Find the content panel spawned by the ResultToolWindowFactory and create ResultToolWindowManager object.
        Content contentPanel = toolWindow.getContentManager().findContent("Code Smell Detection");
        ResultToolWindowManager resultTWManager = (ResultToolWindowManager) contentPanel.getComponent();

        resultTWManager.updateContent(issueList, file);

        // Activate the Tool Window if it is currently closed.
        if (!toolWindow.isActive()) {
            toolWindow.activate(null);
        }
    }
}
