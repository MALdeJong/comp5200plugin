/**
 *  @author Maaike de Jong
 *  The contents of this class were written as part of
 *  the COMP5200 Final Year Project Module.
 */
package com.ResultPane;

import com.CodeSmellDetector.CodeElement;
import com.intellij.openapi.editor.*;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.SimpleToolWindowPanel;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.wm.ToolWindow;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

/**
 * This manager class facilitates all interaction
 * with the plugin's ResultToolWindow layout.
 */
public class ResultToolWindowManager extends SimpleToolWindowPanel {
    private final ResultToolWindow resultToolWindow;
    private VirtualFile issueFileLocation;
    private List<CodeElement> issueList;
    private final DefaultListModel<String> listModel = new DefaultListModel<>();

    /**
     * Constructor for ResultToolWindowManager. It initialises a ResultToolWindow with
     * the layout contained in the ResultToolWindow data class.
     *
     * @param toolWindow The tool window provided by a ToolWindowFactory
     * @param project    Currently opened project
     */
    public ResultToolWindowManager(ToolWindow toolWindow, Project project) {
        super(true, false);
        resultToolWindow = new ResultToolWindow(toolWindow);
        this.add(resultToolWindow.getToolWindowContentPanel());
        makeMouseListener(project);
    }

    /**
     * This function redraws the custom tool window
     * to display the code elements provided in list
     * format.
     *
     * @param issueList The list of code elements to be displayed in the tool window.
     * @param file      The file from which the code elements were obtained.
     */
    public void updateContent(List<CodeElement> issueList, VirtualFile file) {
        setIssueFileLocation(file);
        setIssueList(issueList);
        setListModel();
        setLabel(this.issueList.size(), file.getPresentableName());
        setList();
        this.repaint();
    }

    /**
     * This function creates a list of all CodeElement identifiers
     * contained within the issueList.
     */
    private void setListModel() {
        if (listModel != null)
            listModel.clear();

        if (issueList.size() > 0) {
            List<String> contentIdentifierStringList = new ArrayList<>();   // Temporary list to store the CodeElement identifiers.
            issueList.forEach(c -> {                                        // For each CodeElement in the issue list...
                contentIdentifierStringList.add(c.getElementIdentifier());  // Obtain the CodeElement's identifier, and add it to the temporary list.
            });
            listModel.addAll(contentIdentifierStringList);                  // Add the list of CodeElement identifiers to the listModel for display.
        }
    }

    /**
     * This function hands the result tool window's JList
     * the list of CodeElement identifiers to display.
     */
    private void setList() {
        resultToolWindow.getResultList().setModel(listModel);
    }

    /**
     * This function sets the result tool window's JLabel.
     * @param resultNum Number of CodeElements with a code smell.
     * @param className Name of the class that the CodeElement with a code smell were found in.
     */
    private void setLabel(int resultNum, String className) {
        if (resultNum != 1)
            resultToolWindow.getResultLabel().setText("Found " + resultNum + " results in " + className);
        else
            resultToolWindow.getResultLabel().setText("Found " + resultNum + " result in " + className);
        resultToolWindow.getResultLabel().repaint();
    }

    private void setIssueList(List<CodeElement> qualityIssues) {
        this.issueList = qualityIssues;
    }

    private void setIssueFileLocation(VirtualFile file) {
        this.issueFileLocation = file;
    }

    /**
     * This function initialises the result tool window's mouselistener
     * to facilitate editor navigation to selected code elements.
     *
     * @param project The project that is currently open to allow Editor navigation.
     */
    private void makeMouseListener(Project project) {
        MouseListener mouseListener = new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {

                int selectedOption = resultToolWindow.getResultList().getSelectedIndex();   // Find the index of the user selected CodeElement's identifier.
                CodeElement selectedIssue = issueList.get(selectedOption);                      // Then use it to obtain the corresponding CodeElement object.

                FileEditorManager.getInstance(project).openFile(issueFileLocation, true);   // Navigate to the Java class containing our selected CodeElement.
                Editor editor = FileEditorManager.getInstance(project).getSelectedTextEditor();      // Obtain the editor for caret access.

                assert editor != null;
                CaretModel caretModel = editor.getCaretModel();
                Caret thisCaret = caretModel.getCurrentCaret();

                thisCaret.moveToLogicalPosition(selectedIssue.getStartPos());   // Move the caret to the starting coordinates of the selected CodeElement.
                thisCaret.setSelection(                                         // Highlight the CodeElement.
                        editor.logicalToVisualPosition(selectedIssue.getStartPos()), editor.logicalPositionToOffset(selectedIssue.getStartPos()),
                        editor.logicalToVisualPosition(selectedIssue.getEndPos()), editor.logicalPositionToOffset(selectedIssue.getEndPos())
                );
                ScrollingModel scroller = editor.getScrollingModel();           // Obtain the editor's ScrollingModel.
                scroller.scrollToCaret(ScrollType.CENTER);                      // Put the selected code element in the centre of the screen.

            }
        };
        resultToolWindow.getResultList().addMouseListener(mouseListener);
    }
}
