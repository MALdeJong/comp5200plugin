/**
 *  @author Maaike de Jong
 *  The contents of this class were written as part of
 *  the COMP5200 Final Year Project Module.
 */
package com.ResultPane;

import com.intellij.openapi.wm.ToolWindow;

import javax.swing.*;

public class ResultToolWindow {
    private JPanel toolWindowContentPanel;
    private JList resultList;
    private JLabel resultLabel;

    public ResultToolWindow(ToolWindow toolwindow) { }

    public JList getResultList(){return this.resultList;}
    public JLabel getResultLabel(){return this.resultLabel;}
    public JPanel getToolWindowContentPanel() {
        return toolWindowContentPanel;
    }
}

