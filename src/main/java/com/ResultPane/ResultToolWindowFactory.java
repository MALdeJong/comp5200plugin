/**
 * @author Maaike de Jong
 * The contents of this class were written as part of
 * the COMP5200 Final Year Project Module.
 */
package com.ResultPane;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowFactory;
import com.intellij.openapi.wm.impl.content.ToolWindowContentUi;
import com.intellij.ui.content.Content;
import com.intellij.ui.content.ContentFactory;
import org.jetbrains.annotations.NotNull;

/**
 * Sets the initial layout and content of the ResultToolWindow
 */
public class ResultToolWindowFactory implements ToolWindowFactory {

    /**
     *
     * Spawn a ResultToolWindowManager to set and
     * manage the content of the ResultToolWindow
     *
     * @param project       The project currently open in the editor.
     * @param toolWindow    The ToolWindow to add content to.
     */
    @Override
    public void createToolWindowContent(@NotNull Project project, @NotNull ToolWindow toolWindow) {
        ResultToolWindowManager resultPanel = new ResultToolWindowManager(toolWindow, project);

        ContentFactory contentFactory = ContentFactory.SERVICE.getInstance();
        Content contentPanel = contentFactory.createContent(resultPanel, "Code Smell Detection", false);

        toolWindow.getComponent().putClientProperty(ToolWindowContentUi.HIDE_ID_LABEL, "true");
        toolWindow.getContentManagerIfCreated().addContent(contentPanel);
    }
}