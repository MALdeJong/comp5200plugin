/**
 *  @author Maaike de Jong
 *  The contents of this class were written as part of
 *  the COMP5200 Final Year Project Module.
 */
package com.CodeSmellDetector;

import com.CodeSmellDetector.Visitors.MethodCollector;
import com.github.javaparser.ParserConfiguration;
import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;

import java.io.File;
import java.nio.file.Path;
import java.util.List;

/**
 * This class is an implementation of the SmellSniffer.
 * Given a java class, it will sniff out all methods that bear the long function code smell.
 */
public class LongFunctionSmellSniffer extends SmellSniffer {

    /**
     * Int representation of the maximum number of lines
     * a function can have before it is considered to bear
     * the long function code smell.
     */
    private int codeLinesThreshold = 30;

    public LongFunctionSmellSniffer() {
        super("Long Method");
    }

    /**
     * This method takes a list of CodeElements and detects
     * which of its items bear the Long Function code smell.
     *
     * @param elementList A list of CodeElement objects
     */
    @Override
    protected void detectSmellsInList(List<CodeElement> elementList) {
        if (elementList.size() <= 0) {
            return;
        }
        elementList.forEach(element -> {                                                 // For each method in the list, count its number of lines.
            if (countMethodLines(element.getElementContent()) > codeLinesThreshold) {    // If the number of lines exceeds the threshold, it has the long function code smell.
                issueList.add(element);
            }
        });
    }

    /**
     * This method calls selected visitors on a Java class' AST
     * to find and return all of its methods.
     *
     * @param cu            A CompilationUnit representing the root of a Java class' AST.
     * @param elementList   A list that can hold CodeElement objects
     * @return              The list of CodeElements, now holding all methods found in the Java class' AST.
     */
    @Override
    protected List<CodeElement> callVisitors(CompilationUnit cu, List<CodeElement> elementList) {
        MethodCollector methodCollector = new MethodCollector();
        methodCollector.visit(cu, elementList);
        return elementList;
    }

    /**
     * This method takes the Path to a Java file and parses it.
     * Code comments will be ignored.
     *
     * @param javaFileLocation The Path to the Java file that is to be parsed.
     * @return                 A CompilationUnit object that holds the root to the AST
     * @throws Exception       Will throw an Exception if parsing failed.
     */
    @Override
    protected CompilationUnit createCompilationUnit(Path javaFileLocation) throws Exception {
        ParserConfiguration parserConfiguration = new ParserConfiguration()         //Configure the parser to ignore comments.
                .setAttributeComments(false);
        StaticJavaParser.setConfiguration(parserConfiguration);
        return StaticJavaParser.parse(new File(String.valueOf(javaFileLocation)));  // Parse the chosen Java file.
    }

    /**
     * This method takes a String representation of a method's body and counts its number of lines.
     * Any line with fewer than 4 characters will not be counted.
     *
     * @param methodBody The String representation of a method
     *                   whose number of lines are to be counted
     * @return           The method's length in lines.
     */
    private int countMethodLines(String methodBody) {
        // Count the lines in the methodBody, ignoring any line that is shorter than 3 character in length. -1 to ignore method name line.
        int count = (int) methodBody.lines().filter(line -> line.length() > 3).count() -1;
        if (count < 0)
            count = 0;
        return count;
    }

    /**
     * This method will change the line threshold used to
     * determine at what point a method is considered to
     * have the long function code smell
     *
     * @param codeLinesThreshold   New maximum number of lines
     */
    public void setCodeLinesThreshold(int codeLinesThreshold) {
        this.codeLinesThreshold = codeLinesThreshold;
    }
}
