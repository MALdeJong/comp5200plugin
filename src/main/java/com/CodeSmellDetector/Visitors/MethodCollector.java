/**
 *  @author Maaike de Jong
 *  The contents of this class were written as part of
 *  the COMP5200 Final Year Project Module.
 */
package com.CodeSmellDetector.Visitors;

import com.CodeSmellDetector.CodeElement;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;

import java.util.List;

/**
 * This Visitor class will visit all MethodDeclarations in an AST,
 * then add them to a list of CodeElement objects.
 */
public class MethodCollector extends VoidVisitorAdapter<List> {
    @Override
    public void visit(MethodDeclaration methodDeclaration, List issueList) {
        super.visit(methodDeclaration, issueList);
        issueList.add(new CodeElement(methodDeclaration));
    }
}
