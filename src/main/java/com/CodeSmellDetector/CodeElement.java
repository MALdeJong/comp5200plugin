/**
 *  @author Maaike de Jong
 *  The contents of this class were written as part of
 *  the COMP5200 Final Year Project Module.
 */
package com.CodeSmellDetector;

import com.github.javaparser.Position;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.intellij.openapi.editor.LogicalPosition;

import java.util.Optional;

/**
 * The CodeElement is a data class that offers a generalised representation
 * of the different code elements present within Java ASTs.
 *
 */
public class CodeElement {
    private String foundInClass;
    private LogicalPosition startPos;
    private LogicalPosition endPos;
    private String elementIdentifier;
    private String elementContent;

    public CodeElement(MethodDeclaration methodDeclaration) {
        setFoundInClass(methodDeclaration.getClass().toString());
        setElementIdentifier(methodDeclaration.getNameAsString() + "()");
        setElementContent(methodDeclaration.toString());
        setStartPos(findPosition(methodDeclaration.getBegin()));
        setEndPos(findPosition(methodDeclaration.getEnd()));
    }

    // Converts Optional<Position> variables into LogicalPosition variables that can be used by the IDEA's Editor.
    private LogicalPosition findPosition(Optional<Position> pos) {
        Position position = (Position) pos.get();
        return new LogicalPosition(position.line - 1, position.column - 1);
    }

    private void setFoundInClass(String foundInClass) {
        this.foundInClass = foundInClass;
    }

    private void setElementIdentifier(String elementIdentifier) {
        this.elementIdentifier = elementIdentifier;
    }

    private void setElementContent(String elementContent) {
        this.elementContent = elementContent;
    }

    private void setStartPos(LogicalPosition position) {
        startPos = position;
    }

    private void setEndPos(LogicalPosition position) {
        endPos = position;
    }

    public String getFoundInClass() {
        return this.foundInClass;
    }

    public String getElementIdentifier() {
        return this.elementIdentifier;
    }

    public String getElementContent() {
        return this.elementContent;
    }

    public LogicalPosition getStartPos() {
        return this.startPos;
    }

    public LogicalPosition getEndPos() {
        return this.endPos;
    }

}
