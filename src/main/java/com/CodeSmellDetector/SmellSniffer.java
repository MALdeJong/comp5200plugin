/**
 *  @author Maaike de Jong
 *  The contents of this class were written as part of
 *  the COMP5200 Final Year Project Module.
 */
package com.CodeSmellDetector;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;

import java.io.File;
import java.nio.file.Path;

import java.util.ArrayList;
import java.util.List;

/**
 * This abstract class holds all functionality needed to create
 * custom SmellSniffers to detect chosen code smells.
 */
public abstract class SmellSniffer {
    protected List<CodeElement> issueList = new ArrayList<CodeElement>();   // List of all CodeElements with a code smell.
    protected String codeSmell;                                   // The Code Smell the implementing class is designed to detect

    public SmellSniffer(String thisSmell) {
        setCodeSmell(thisSmell);
    }

    /**
     * This function will analyse a provided Java class for
     * the selected code smell.
     *
     * @param filePath Path to a Java class
     */
    public void analyse(Path filePath) {
        List<CodeElement> elementList = new ArrayList<>();

        try {
            elementList = callVisitors(createCompilationUnit(filePath), elementList);
        } catch (Exception e) {
            e.printStackTrace();
        }

        detectSmellsInList(elementList);
    }

    /**
     * This method should a list of CodeElements and detect
     * which of its items bear the implementation's code smell.
     *
     * @param elementList A list of CodeElement objects
     */
    protected abstract void detectSmellsInList(List<CodeElement> elementList);

    /**
     * This method takes the Path to a Java file and parses it.
     *
     * @param javaFileLocation The Path to the Java file that is to be parsed.
     * @return                 A CompilationUnit object that holds the root to the AST
     * @throws Exception       Will throw an Exception if parsing failed.
     */
    protected CompilationUnit createCompilationUnit(Path javaFileLocation) throws Exception {
        return StaticJavaParser.parse(new File(String.valueOf(javaFileLocation)));
    }

    /**
     * Implement this method to call the necessary visitor implementations
     * on a parsed Java class.
     *
     * @param cu            CompilationUnit of the parsed Java class.
     * @param elementList   The list in which all CodeElements identified by visitors can be stored.
     * @return              The elementList after visitors have stored CodeElements in it.
     */
    protected abstract List<CodeElement> callVisitors(CompilationUnit cu, List<CodeElement> elementList);

    public List<CodeElement> getIssueList() {
        return this.issueList;
    }

    public String getCodeSmell() {
        return this.codeSmell;
    }

    private void setCodeSmell(String codeSmell) {
        this.codeSmell = codeSmell;
    }
}
